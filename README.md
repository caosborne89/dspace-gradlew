# Dspace-gradlew Build

## Requirments

* Maven (only tested with 3.5)
* Ant (only tested with 1.9.6)
* PostgreSQL (only tested with 9.5)

## How to get started
Note: Instructions on setting up the database and the local.cfg file are directly taken from the [dspace duraspace site](https://wiki.duraspace.org/display/DSDOC6x/Installing+DSpace)

1. Create databse user (in the case below the user `dpsace` is being created)

    ```
    createuser --username=postgres --no-superuser --pwprompt dspace
    ```


2.  Create the database (in the case below the database name is `dspace`)

    ``` 
    createdb --username=postgres --owner=dspace --encoding=UNICODE dspace
    ```


3. Enable the pgcrypto extension on your new dspace database.  Login to the database as a superuser, and enable the pgcrypto extension on this database.
    
    ```
    psql --username=postgres dspace -c "CREATE EXTENSION pgcrypto;"
    ```


4. Copy local.example.cfg to local.cfg and fill in it in accordingly
(NOTE: Settings followed with an asterisk (*) are highly recommended, while all others are optional during initial installation and may be customized at a later time)


    * dspace.dir* - must be set to the [dspace] (installation) directory  (NOTE: On Windows be sure to use forward slashes for the directory path!  For example: "C:/dspace" is a valid path for Windows.)
    * dspace.hostname - fully-qualified domain name of web server (or "localhost" if you just want to run DSpace locally for now)
    * dspace.baseUrl* - complete URL of this server's DSpace home page (including port), but without any context eg. /xmlui, /oai, etc.
    * dspace.name - "Proper" name of your server, e.g. "My Digital Library".
    * solr.server* - complete URL of the Solr server. DSpace makes use of Solr for indexing purposes.  
    * default.language - Default language for all metadata values (defaults to "en_US")
    * db.url* - The full JDBC URL to your database (examples are provided in the local.cfg.EXAMPLE) 
    * db.driver* - Which database driver to use, based on whether you are using PostgreSQL or Oracle 
    * db.dialect* - Which database dialect to use, based on whether you are using PostgreSQL or Oracle
    * db.username* - the database username used in the previous step.
    * db.password* - the database password used in the previous step.
    * db.schema* - the database scheme to use (examples are provided in the local.cfg.EXAMPLE)
    * mail.server - fully-qualified domain name of your outgoing mail server.
    * mail.from.address - the "From:" address to put on email sent by DSpace.
    * mail.feedback.recipient - mailbox for feedback mail.
    * mail.admin - mailbox for DSpace site administrator.
    * mail.alert.recipient - mailbox for server errors/alerts (not essential but very useful!)
    * mail.registration.notify- mailbox for emails when new users register (optional)


5. From the root of the repo run
    ```
    ./gradlew fullBuild
    ```


## Known Issues

* Currently the tomcat container is not working